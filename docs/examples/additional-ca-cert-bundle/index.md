# Use a custom SSL CA certificate authority

You can use the `ADDITIONAL_CA_CERT_BUNDLE` [CI/CD variable](http://docs.gitlab.com/ee/ci/variables) to configure a custom SSL CA certificate authority,
which is used to verify the peer when the `release-cli` creates a release through the API using HTTPS with custom certificates.
The `ADDITIONAL_CA_CERT_BUNDLE` value should contain the
[text representation of the X.509 PEM public-key certificate](https://tools.ietf.org/html/rfc7468#section-5.1)
or the `path/to/file` containing the certificate authority.
For example, to configure this value in the `.gitlab-ci.yml` file, use the following:

```yaml
release:
  variables:
    ADDITIONAL_CA_CERT_BUNDLE: |
        -----BEGIN CERTIFICATE-----
        MIIGqTCCBJGgAwIBAgIQI7AVxxVwg2kch4d56XNdDjANBgkqhkiG9w0BAQsFADCB
        ...
        jWgmPqF3vUbZE0EyScetPJquRFRKIesyJuBFMAs=
        -----END CERTIFICATE-----
  script:
    - echo "Create release"
  release:
    name: 'My awesome release'
    tag_name: '$CI_COMMIT_TAG'
```

For the standalone `release-cli` command you can also use the `--additional-ca-cert-bundle` CLI flag.
For example:

```yaml
release:
  script:
    - release-cli --additional-ca-cert-bundle ./path/to/ca.pem create --name 'My awesome release' --tag '$CI_COMMIT_TAG' 
```
