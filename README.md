#  GitLab Release command-line tool

[![build status](https://gitlab.com/gitlab-org/release-cli/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/release-cli/commits/master)

[![coverage report](https://gitlab.com/gitlab-org/release-cli/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/release-cli/commits/master)

The **GitLab Release CLI** consumes instructions in the `:release` node of the `.gitlab-ci.yml` to create a Release object in GitLab Rails. GitLab Release is a CLI application written in [Golang](https://golang.org/)

The GitLab Release CLI is a decoupled utility that may be called by the GitLab Runner, by a third-party CI or directly from the command-line. It uses the CI `Job-Token` to authorize against the GitLab Rails API, which is passed to it by the GitLab Runner.

The CLI can also be called independently, and can still create the Release via Rails API if the `Job-Token` and correct command line params are provided.

## Documentation

The documentation source files can be found under the [docs/](docs/) directory.

## Use GitLab Release CLI

See the [usage](docs/index.md#usage) and some [examples](docs/examples/).
